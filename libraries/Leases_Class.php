<?php

/**
 * Dnsmasq leases class.
 *
 * @category   apps
 * @package    dhcp
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2003-2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/dhcp/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\dhcp;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('dhcp');

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

// Classes
//--------

use \clearos\apps\base\Engine as Engine;
use \clearos\apps\base\File as File;
use \clearos\apps\dhcp\Settings_Class as Settings_Class;
use \clearos\apps\network\Ethers as Ethers;
use \clearos\apps\network\Iface as Iface;
use \clearos\apps\network\Network_Utils as Network_Utils;

clearos_load_library('base/Engine');
clearos_load_library('base/File');
clearos_load_library('dhcp/Settings_Class');
clearos_load_library('network/Ethers');
clearos_load_library('network/Iface');
clearos_load_library('network/Network_Utils');

// Exceptions
//-----------

use \clearos\apps\base\Not_Found_Exception as Not_Found_Exception;
use \clearos\apps\base\Validation_Exception as Validation_Exception;

clearos_load_library('base/Not_Found_Exception');
clearos_load_library('base/Validation_Exception');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Dnsmasq leases class.
 *
 * @category   apps
 * @package    dhcp
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2003-2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/dhcp/
 */

class Leases_Class extends Engine
{
    ///////////////////////////////////////////////////////////////////////////////
    // C O N S T A N T S
    ///////////////////////////////////////////////////////////////////////////////

    const FILE_LEASES = '/var/lib/dnsmasq/dnsmasq.leases';
    const TYPE_STATIC = 'static';
    const TYPE_DYNAMIC = 'dynamic';

    ///////////////////////////////////////////////////////////////////////////////
    // V A R I A B L E S
    ///////////////////////////////////////////////////////////////////////////////

    protected $types = array();

    ///////////////////////////////////////////////////////////////////////////////
    // M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * DHCP leases constructor.
     */

    public function __construct()
    {
        clearos_profile(__METHOD__, __LINE__);

        $this->types = array(
            self::TYPE_STATIC => lang('dhcp_static'),
            self::TYPE_DYNAMIC => lang('dhcp_dynamic')
        );
    }

    /**
     * Adds a static lease to DHCP server.
     *
     * @param string $mac MAC address
     * @param string $ip  IP address
     *
     * @return void
     * @throws Engine_Exception, Validation_Exception
     */

    public function add_static_lease($mac, $ip)
    {
        clearos_profile(__METHOD__, __LINE__);

        Validation_Exception::is_valid($this->validate_mac($mac));
        Validation_Exception::is_valid($this->validate_ip($ip));

        $ethers = new Ethers();

        $ethers->add_ether($mac, $ip);
    }

    /**
     * Deletes a lease from DHCP server.
     *
     * @param string $mac MAC address
     * @param string $ip  IP address
     *
     * @return void
     * @throws Engine_Exception
     */

    public function delete_lease($mac, $ip)
    {
        clearos_profile(__METHOD__, __LINE__);

        $this->_delete_static_lease($mac);
        $this->_delete_dynamic_lease($mac, $ip);
    }

    /**
     * Returns lease information.
     *
     * @param string $mac MAC address
     * @param string $ip  IP address
     *
     * @return array array containing lease data
     * @throws Engine_Exception
     */

    public function get_lease($mac, $ip)
    {
        clearos_profile(__METHOD__, __LINE__);

        $leases = $this->get_leases();

        foreach ($leases as $lease) {
            if (($lease['mac'] === $mac) && ($lease['ip'] === $ip))
                return $lease;
        }
    }

    /**
     * Returns standard lease time options.
     *
     * @return array array lease time options
     * @throws Engine_Exception
     */

    public function get_lease_time_options()
    {
        clearos_profile(__METHOD__, __LINE__);

        $lease_times = array();
        $lease_times[1] = 1 . " " . lang('base_hour');
        $lease_times[2] = 2 . " " . lang('base_hours');
        $lease_times[4] = 4 . " " . lang('base_hours');
        $lease_times[6] = 6 . " " . lang('base_hours');
        $lease_times[12] = 12 . " " . lang('base_hours');
        $lease_times[24] = 24 . " " . lang('base_hours');
        $lease_times[48] = 2 . " " . lang('base_days');
        $lease_times[72] = 3 . " " . lang('base_days');
        $lease_times[96] = 4 . " " . lang('base_days');
        $lease_times[120] = 5 . " " . lang('base_days');
        $lease_times[144] = 6 . " " . lang('base_days');
        $lease_times[168] = 7 . " " . lang('base_days');
        $lease_times[336] = 2 . " " . lang('base_weeks');
        $lease_times[504] = 3 . " " . lang('base_weeks');
        $lease_times[672] = 4 . " " . lang('base_weeks');
        $lease_times[Settings_Class::CONSTANT_UNLIMITED_LEASE] = lang('base_unlimited');

        return $lease_times;
    }

    /**
     * Returns merged list of active and static leases.
     *
     * Lease data is keyed by MAC address, but sorted by IP.
     *
     * @return array array containing lease data
     * @throws Engine_Exception
     */

    public function get_leases()
    {
        clearos_profile(__METHOD__, __LINE__);

        // Pull in MAC address database
        include clearos_app_base('dhcp') . '/deploy/mac_database.php';

        /**
         * The MAC/IP pair in the static leases (/etc/ethers) and active
         * leases (/var/lib/misc/dnsmasq.leases) could be different.  For
         * example, a machine may grab a dynamic lease at first but an
         * administrator could later add a static entry for future use.
         *
         * For this reason, the list of leases is keyed on the MAC/IP pairing.
         * There is a little trickery going on to handle the key.  First,
         * ip2long is used so that 192.168.1.20 comes before
         * 192.168.1.100.  In addition, the MAC address becomes a decimal,
         * e.g. 11:22:33:44:55:66 becomes 0.112233445566.  The unique keys
         * would look similar to 3232236157.112233445566.
         */

        $active = $this->_get_active_leases();
        $static = $this->_get_ethers_data();
        $leases = array();
        $ip_ndx = array();

        foreach ($active as $mac => $details) {
            $key = sprintf("%u.%s", ip2long($details['ip']), hexdec(preg_replace("/\:/", "", $mac)));
            $mac_prefix = strtoupper(substr($mac, 0, 8));

            $leases[$key]['vendor'] = isset($mac_database[$mac_prefix]) ? $mac_database[$mac_prefix] : '';
            $leases[$key]['mac'] = $mac;
            $leases[$key]['ip'] = $details['ip'];
            $leases[$key]['end'] = $details['end'];
            $leases[$key]['is_active'] = TRUE;
            $leases[$key]['type'] = self::TYPE_DYNAMIC;
            $leases[$key]['hostname'] = $details['hostname'];
        }

        foreach ($static as $mac => $details) {
            $key = sprintf("%u.%s", ip2long($details['ip']), hexdec(preg_replace("/\:/", "", $mac)));
            $mac_prefix = strtoupper(substr($mac, 0, 8));

            $leases[$key]['vendor'] = isset($mac_database[$mac_prefix]) ? $mac_database[$mac_prefix] : '';
            $leases[$key]['mac'] = $mac;
            $leases[$key]['ip'] = $details['ip'];
            $leases[$key]['end'] = 0;
            $leases[$key]['is_active'] = TRUE;
            $leases[$key]['type'] = self::TYPE_STATIC;
            $leases[$key]['hostname'] = $details['hostname'];
        }

        ksort($leases);

        return $leases;
    }

    /**
     * Returns static leases.
     *
     * Lease data is keyed by MAC address, but sorted by IP.
     *
     * @return array array containing lease data
     * @throws Engine_Exception
     */

    public function get_static_leases()
    {
        clearos_profile(__METHOD__, __LINE__);

        $leases = $this->get_leases();

        $statics = array();

        foreach ($leases as $lease => $details) {
            if ($details['type'] === self::TYPE_STATIC)
                $statics[$lease] = $details;
        }

        return $statics;
    }

    /**
     * Returns lease types.
     *
     * @return array lease types
     * @throws Engine_Exception
     */

    public function get_types()
    {
        clearos_profile(__METHOD__, __LINE__);

        return $this->types;
    }

    /**
     * Returns state of lease for given MAC address.
     *
     * @param string $mac MAC address
     *
     * @return array array containing lease data
     * @throws Engine_Exception
     */

    public function exists_lease($mac)
    {
        clearos_profile(__METHOD__, __LINE__);

        $leases = $this->get_leases();

        foreach ($leases as $lease) {
            if ($lease['mac'] === $mac)
                return TRUE;
        }

        return FALSE;
    }

    /**
     * Updates an existing lease.
     *
     * @param string $mac  MAC address
     * @param string $ip   IP address
     * @param string $type lease type
     *
     * @return void
     * @throws Engine_Exception, Validation_Exception
     */

    public function update_lease($mac, $ip, $type)
    {
        clearos_profile(__METHOD__, __LINE__);

        Validation_Exception::is_valid($this->validate_mac($mac));
        Validation_Exception::is_valid($this->validate_ip($ip));
        Validation_Exception::is_valid($this->validate_lease_type($type));

        $this->_delete_static_lease($mac);
        $this->_delete_dynamic_lease($mac, $ip);

        if ($type === self::TYPE_STATIC)
            $this->add_static_lease($mac, $ip);
        else
            $this->_add_dynamic_lease($mac, $ip);
    }

    ///////////////////////////////////////////////////////////////////////////////
    // P R I V A T E  M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Adds a dynamiuc lease to DHCP server.
     *
     * @param string $mac MAC address
     * @param string $ip  IP address
     *
     * @return void
     * @throws Engine_Exception, Validation_Exception
     */

    protected function _add_dynamic_lease($mac, $ip)
    {
        clearos_profile(__METHOD__, __LINE__);

        Validation_Exception::is_valid($this->validate_mac($mac));
        Validation_Exception::is_valid($this->validate_ip($ip));

        $file = new File(self::FILE_LEASES);

        if (! $file->exists())
            $file->create('root', 'root', '0644');

        $lease_time = time() + 3600;

        $file->add_lines($lease_time . ' ' . strtolower($mac) . ' ' . $ip . " * *\n");
    }

    /**
     * Deletes a dynamic lease from DHCP server.
     *
     * @param string $mac MAC address
     * @param string $ip  IP address
     *
     * @return void
     * @throws Engine_Exception
     */

    protected function _delete_dynamic_lease($mac, $ip)
    {
        clearos_profile(__METHOD__, __LINE__);

        $file = new File(self::FILE_LEASES);

        // TODO: we may need to match on the IP too
        if ($file->exists()) 
            $file->delete_lines("/[0-9]*\s+$mac\s+/i");
    }

    /**
     * Deletes a static lease from DHCP server.
     *
     * @param string $mac MAC address
     *
     * @return void
     * @throws Engine_Exception
     */

    protected function _delete_static_lease($mac)
    {
        clearos_profile(__METHOD__, __LINE__);

        $ethers = new Ethers();

        try {
            $ethers->delete_ether($mac);
        } catch (Not_Found_Exception $e) {
            // Not fatal
        }
    }

    /**
     * Returns active leases.
     *
     * @access private
     * @return array array containing lease data
     * @throws Engine_Exception
     */

    protected function _get_active_leases()
    {
        clearos_profile(__METHOD__, __LINE__);

        $leases = array();
        $leasefile = new File(self::FILE_LEASES);

        if (! $leasefile->exists())
            return array();

        $leasedata = $leasefile->get_contents_as_array();

        foreach ($leasedata as $line) {
            if (empty($line))
                continue;

            $parts = preg_split('/[\s]+/', $line);

            $key = $parts[1];

            $leases[$key]['end'] = isset($parts[0]) ? $parts[0] : "";
            $leases[$key]['ip'] = isset($parts[2]) ? $parts[2] : "";
            $leases[$key]['hostname'] = isset($parts[3]) ? $parts[3] : "";
        }

        return $leases;
    }

    /**
     * Returns static leases from /etc/ethers.
     *
     * @access private
     * @return array lease data keyed by MAC and sorted by IP
     * @throws Engine_Exception
     */

    protected function _get_ethers_data()
    {
        clearos_profile(__METHOD__, __LINE__);

        $leases = array();
        $ip_ndx = array();

        $dnsmasq = new Settings_Class();
        $read_ethers = $dnsmasq->get_read_ethers_state();

        // Bail if read-ethers feature is disabled
        if (!$read_ethers)
            return array();

        $ethers = new Ethers();
        $mac_ip_pairs = $ethers->get_ethers();

        foreach ($mac_ip_pairs as $mac => $host_or_ip) {

            // Find a hostname for IP address entries
            // Find an IP for hostname entries

            if (Network_Utils::is_valid_ip($host_or_ip)) {
                $ip = $host_or_ip;
                $hostname = gethostbyaddr($host_or_ip);
                if ($hostname == $host_or_ip)    
                    $hostname = '';
            } else {
                $hostname = $host_or_ip;
                $ip = gethostbyname($host_or_ip);
                if ($ip == $host_or_ip)    
                    $ip = '';
            }

            // Keep an index to sort by IP

            $ip_long = sprintf("%u", ip2long($ip));
            $ip_ndx[$ip_long] = $mac;

            $leases[$mac]['ip'] = $ip;
            $leases[$mac]['hostname'] = $hostname;
        }

        ksort($ip_ndx);

        $sorted_leases = array();

        foreach ($ip_ndx as $ip => $mac) {
            $sorted_leases[$mac]['ip'] = $leases[$mac]['ip'];
            $sorted_leases[$mac]['hostname'] = $leases[$mac]['hostname'];
        }

        return $sorted_leases;
    }

    ///////////////////////////////////////////////////////////////////////////////
    // V A L I D A T I O N
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Validates IP.
     *
     * @param string $ip IP address
     *
     * @return string error message if IP is invalid
     */

    public function validate_ip($ip)
    {
        clearos_profile(__METHOD__, __LINE__);

        if (! Network_Utils::is_valid_ip($ip))
            return lang('network_ip_invalid');
    }

    /**
     * Validates lease time.
     *
     * @param integer $time lease time
     *
     * @return string error message if lease time is invalid
     */

    public function validate_lease_time($time)
    {
        clearos_profile(__METHOD__, __LINE__);

        if (! (preg_match("/^\d+$/", $time) || ($time === Settings_Class::CONSTANT_UNLIMITED_LEASE)))
            return lang('dhcp_lease_time_invalid');
    }

    /**
     * Validates lease type.
     *
     * @param string $type lease type
     *
     * @return string error message if lease type is invalid
     */

    public function validate_lease_type($type)
    {
        clearos_profile(__METHOD__, __LINE__);

        if (! array_key_exists($type, $this->types))
            return lang('dhcp_lease_type_invalid');
    }

    /**
     * Validates MAC address
     *
     * @param string  $mac              MAC address
     * @param boolean $check_uniqueness checks to see if MAC is unique
     *
     * @return string error message if MAC address is invalid
     */

    public function validate_mac($mac, $check_uniqueness = FALSE)
    {
        clearos_profile(__METHOD__, __LINE__);

        if (! Network_Utils::is_valid_mac($mac, TRUE))
            return lang('network_mac_address_invalid');

        if ($check_uniqueness && $this->exists_lease($mac))
            return lang('dhcp_mac_address_already_exists');
    }
}
