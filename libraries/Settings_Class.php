<?php

/**
 * Dnsmasq DHCP settings class.
 *
 * @category   apps
 * @package    dhcp
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2003-2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/dhcp/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\dhcp;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('dhcp');

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

// Classes
//--------

use \clearos\apps\base\Engine as Engine;
use \clearos\apps\base\File as File;
use \clearos\apps\network\Network_Utils as Network_Utils;

clearos_load_library('base/Engine');
clearos_load_library('base/File');
clearos_load_library('network/Network_Utils');

// Exceptions
//-----------

use \clearos\apps\base\Validation_Exception as Validation_Exception;

clearos_load_library('base/Validation_Exception');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Dnsmasq DHCP settings class.
 *
 * @category   apps
 * @package    dhcp
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2003-2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/dhcp/
 */

class Settings_Class extends Engine
{
    ///////////////////////////////////////////////////////////////////////////////
    // C O N S T A N T S
    ///////////////////////////////////////////////////////////////////////////////

    const FILE_CONFIG = '/etc/dnsmasq.conf';
    const CONSTANT_UNLIMITED_LEASE = 'infinite';

    ///////////////////////////////////////////////////////////////////////////////
    // V A R I A B L E S
    ///////////////////////////////////////////////////////////////////////////////

    protected $is_loaded = FALSE;
    protected $config = array();

    ///////////////////////////////////////////////////////////////////////////////
    // M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Dnsmasq DHCP settings constructor.
     */

    public function __construct()
    {
        clearos_profile(__METHOD__, __LINE__);
    }

    /**
     * Returns authoritative state.
     *
     * @return boolean TRUE if authoritative
     * @throws Engine_Exception
     */

    public function get_authoritative_state()
    {
        clearos_profile(__METHOD__, __LINE__);

        if (! $this->is_loaded)
            $this->_load_config();

        if (isset($this->config['dhcp-authoritative']['line'][1]))
            return TRUE;
        else
            return FALSE;
    }

    /**
     * Returns default domain name.
     *
     * @return string default domain name
     * @throws Engine_Exception
     */

    public function get_domain_name()
    {
        clearos_profile(__METHOD__, __LINE__);

        if (! $this->is_loaded)
            $this->_load_config();

        if (isset($this->config['domain']['line'][1]))
            return $this->config['domain']['line'][1];
        else
            return '';
    }

    /**
     * Returns read ethers state.
     *
     * @return boolean TRUE if read ethers enabled
     * @throws Engine_Exception
     */

    public function get_read_ethers_state()
    {
        clearos_profile(__METHOD__, __LINE__);

        if (! $this->is_loaded)
            $this->_load_config();

        if (isset($this->config['read-ethers']['line'][1]))
            return TRUE;
        else
            return FALSE;
    }

    /**
     * Sets state of authoritative flag.
     *
     * @param boolean $state authoritative state
     *
     * @return void
     * @throws Engine_Exception
     */

    public function set_authoritative_state($state)
    {
        clearos_profile(__METHOD__, __LINE__);

        Validation_Exception::is_valid($this->validate_authoritative_state($state));

        if (! $this->is_loaded)
            $this->_load_config();

        // Cleans out invalid duplicates
        if (isset($this->config['dhcp-authoritative']))
            unset($this->config['dhcp-authoritative']);

        if ($state)
            $this->config['dhcp-authoritative']['line'][1] = '';

        $this->_save_config();
    }

    /**
     * Sets global domain name.
     *
     * @param string $domain domain name
     *
     * @return void
     * @throws Engine_Exception
     */

    public function set_domain_name($domain)
    {
        clearos_profile(__METHOD__, __LINE__);

        Validation_Exception::is_valid($this->validate_domain($domain));

        // Cleans out invalid duplicates
        if (! $this->is_loaded)
            $this->_load_config();

        if ($this->config['domain'])
            unset($this->config['domain']);

        $this->config['domain']['line'][1] = $domain;

        $this->_save_config();
    }

    ///////////////////////////////////////////////////////////////////////////////
    // V A L I D A T I O N
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Validates authoritative state
     *
     * @param boolean $state authoritative state
     *
     * @return string error message if authoritative state is invalid
     */

    public function validate_authoritative_state($state)
    {
        clearos_profile(__METHOD__, __LINE__);

        if (! clearos_is_valid_boolean($state))
            return lang('dhcp_authoritative_state_invalid');
    }

    /**
     * Validates DHCP domain name.
     *
     * @param string $domain domain
     *
     * @return string error message if domain is invalid
     */

    public function validate_domain($domain)
    {
        clearos_profile(__METHOD__, __LINE__);

        if (! Network_Utils::is_valid_domain($domain))
            return lang('dhcp_domain_invalid');
    }

    ///////////////////////////////////////////////////////////////////////////////
    // P R I V A T E  M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Loads configuration file.
     *
     * The Dnsmasq configuration file can have multiple keys with the same
     * value.  For instance:
     *
     * - dhcp-option=eth1,44,192.168.2.16
     * - dhcp-option=eth1,1,255.255.255.0
     * - dhcp-option=eth1,28,192.168.2.255
     *
     * The config() array is in the following format to deal with this:
     * 
     * config[key][count]
     * config[key][line][line_index]
     *
     * In our example, this would look like:
     *
     * config['dhcp-option']['count'] = 3
     * config['dhcp-option']['line'][1] = eth1,44,192.168.2.16
     * config['dhcp-option']['line'][2] = eth1,1,255.255.255.0
     * config['dhcp-option']['line'][3] = eth1,28,192.168.2.255
     *
     * @access private
     * @return void
     * @throws Engine_Exception
     */

    protected function _load_config()
    {
        clearos_profile(__METHOD__, __LINE__);

        $lines = array();

        $dnsmasq_file = new File(self::FILE_CONFIG);

        $lines = $dnsmasq_file->get_contents_as_array();

        $matches = array();

        foreach ($lines as $line) {
            if (preg_match("/^#/", $line) || preg_match("/^\s*$/", $line)) {
                continue;
            } else if (preg_match("/(.*)=(.*)/", $line, $matches)) {
                $key = $matches[1];
                $value = $matches[2];
            } else {
                $key = trim($line);
                $value = '';
            }

            if (isset($this->config[$key]['count']))
                $this->config[$key]['count']++;
            else
                $this->config[$key]['count'] = 1;

            // $count is just to make code more readable
            $count = $this->config[$key]['count'];
            $this->config[$key]['line'][$count] = $value;
        }

        $this->is_loaded = TRUE;
    }

    /**
     * Save configuration changes.
     *
     * @access private
     * @return void
     * @throws Engine_Exception
     */

    protected function _save_config()
    {
        clearos_profile(__METHOD__, __LINE__);

        $dnsmasq_file = new File(self::FILE_CONFIG);

        // Go through the existing configuration files and save
        // any user-created comments.

        $dnsmasq_comments = array();

        if ($dnsmasq_file->exists()) {
            $lines = $dnsmasq_file->get_contents_as_array();
            foreach ($lines as $line) {
                if (preg_match("/^#/", $line))
                    $dnsmasq_comments[] = $line;
            }
        }

        $dnsmasq_lines = array();

        // Always enable read-ethers for now
        $this->config['read-ethers']['line'][1] = '';

        foreach ($this->config as $key => $details) {
            foreach ($details['line'] as $lineno => $value) {
                if ($value || ($value === '0'))
                    $line = "$key=$value";
                else
                    $line = "$key";

                $dnsmasq_lines[] = $line;
            }
        }

        // Append any user-created comments to the file.

        $dnsmasq_lines = array_merge($dnsmasq_lines, $dnsmasq_comments);

        // Write out the files
        //--------------------

        if (! $dnsmasq_file->exists())
            $dnsmasq_file->create('root', 'root', '0644');

        sort($dnsmasq_lines);

        $dnsmasq_file->dump_contents_from_array($dnsmasq_lines);

        // Reset our internal data structures
        //-----------------------------------

        $this->is_loaded = FALSE;
        $this->config = array();
    }
}
