To update the MAC address database, go into the packaging folder then `wget -N http://standards-oui.ieee.org/oui.txt`. If a newer file is downloaded, then run `./update_mac_db.php > ../deploy/mac_database.php`

The same mac_database.php file should then be used in app-network-map.
