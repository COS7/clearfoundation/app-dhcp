<?php

/**
 * DHCP API controller.
 *
 * @category   apps
 * @package    dhcp
 * @subpackage rest-api
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/dhcp/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * DHCP API controller.
 *
 * @category   apps
 * @package    dhcp
 * @subpackage rest-api
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/dhcp/
 */

class DHCP extends ClearOS_REST_Controller
{
    /**
     * DHCP overview.
     *
     * @return API response
     */

    function index_get()
    {
        try {
            $data['capabilities'] = [
                '/api/v1/dhcp/settings' => 'DHCP settings',
                '/api/v1/dhcp/server' => 'DHCP server',
                '/api/v1/dhcp/subnets' => 'DHCP subnets',
                '/api/v1/dhcp/leases' => 'DHCP leases',
            ];

            $this->respond_success($data);
        } catch (\Exception $e) {
            $this->exception_handler($e);
        }
    }
}
